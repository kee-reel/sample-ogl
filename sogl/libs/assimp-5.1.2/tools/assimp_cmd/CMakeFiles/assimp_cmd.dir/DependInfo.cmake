# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CompareDump.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/CompareDump.cpp.o"
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/Export.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/Export.cpp.o"
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/ImageExtractor.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/ImageExtractor.cpp.o"
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/Info.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/Info.cpp.o"
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/Main.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/Main.cpp.o"
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/WriteDump.cpp" "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/tools/assimp_cmd/CMakeFiles/assimp_cmd.dir/WriteDump.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASSIMP_BUILD_NO_C4D_IMPORTER"
  "ASSIMP_BUILD_NO_M3D_EXPORTER"
  "ASSIMP_BUILD_NO_M3D_IMPORTER"
  "ASSIMP_BUILD_NO_OWN_ZLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "."
  "code"
  "."
  "tools/assimp_cmd"
  "code/../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/keereel/proj/sample-ogl/libs/assimp-5.1.2/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
